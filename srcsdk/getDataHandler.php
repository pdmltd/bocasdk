<?php

namespace Boca\Client;

require_once  __DIR__ .  '/../vendor/autoload.php';

use Illuminate\Http\Request;
use Illuminate\Http\Response;

if(isset( $_REQUEST )) {
    $sdk = new Sdk;
    $request = new Request($_REQUEST);
    $result = $sdk->validateUser($request);
    echo json_encode($result);
}