<?php 

namespace Boca\Client;

require_once  __DIR__ .  '/../vendor/autoload.php';

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7; 
use Symfony\Component\Yaml\Yaml;


final class Sdk
{

    public function __construct()
    {
        $this->config_credentials   = Yaml::parseFile('../config/boca-credentials.yaml');
        $this->base_url             = $this->config_credentials['base_url'];
        $this->token_path           = $this->config_credentials['token_path'];
        $this->api_path             = $this->config_credentials['api_path'];
        $this->grant_type           = $this->config_credentials['grant_type'];
        $this->client_id            = $this->config_credentials['client_id'];
        $this->client_secret        = $this->config_credentials['client_secret'];
        $this->dg_uuid              = $this->config_credentials['dailygame_uuid'];
        // $this->brand_uuid           = $this->config_credentials['brand_uuid'];
        $this->access_token         = $this->get_access_token();
    }

    /**
     * Get Access tokens call to Boca Services
     */
    private function get_access_token()
    {
        $http = new Client();
        $response = $http->post($this->base_url.'/'.$this->token_path, [
            'form_params' => [
                'grant_type' => $this->grant_type,
                'client_id' => $this->client_id,
                'client_secret' => $this->client_secret,
            ],
        ]);
        $this->access_token = json_decode((string) $response->getBody(), true)['access_token'];
        return $this->access_token;
    }

    /**
     * Validate User call to Boca Services
     */
    public function validateUser(Request $request)
    { 
        $client = new Client(['base_uri' => $this->base_url.'/'.$this->api_path]);
        $this->brand_uuid = $this->config_credentials[$request->skinId];
        $headers = [
            'Authorization' => 'Bearer ' . $this->access_token,
            'Accept'        => 'application/json',
        ];
        $response = $client->request('POST', 'dailygame/'.$this->dg_uuid .'/player/validate', [
            'headers' => $headers,
            'form_params' => [
                'network_player_id' => $request->username,
                'brand_uuid' =>  $this->brand_uuid
            ]
        ]);

        $response_decode = json_decode((string) $response->getBody(), true);

        if(!$response_decode['status']) {
            return $response_decode;
        }
        
        $player_uuid = $response_decode['player_uuid'];
        return $this->initGame($player_uuid);
    }

    /**
     * Initialise Game call to Boca Services
     */
    public function initGame($player_uuid, $adapter_type = "scratchard")
    {
        $client = new Client(['base_uri' => $this->base_url.'/'.$this->api_path]);
        $headers = [
            'Authorization' => 'Bearer ' . $this->access_token,
            'Accept'        => 'application/json',
        ];
        $response = $client->request('POST', 'dailygame/'.$this->dg_uuid .'/initiate', [
            'headers' => $headers,
            'form_params' => [
                'player_uuid' => $player_uuid,
                'brand_uuid' =>  $this->brand_uuid
            ]
        ]);

        $sdk_adapter = new Adapter;
        return $sdk_adapter->{$adapter_type}($response);
    }

    /**
     * Initialise Game call to Boca Services
     */
    public function completeGame(Request $request)
    {
        $client = new Client(['base_uri' =>  $this->base_url.'/'.$this->api_path]);
        $headers = [
            'Authorization' => 'Bearer ' . $this->access_token,
            'Accept'        => 'application/json',
        ];
        $response = $client->request('POST', 'game/play/complete', [
            'headers' => $headers,
            'form_params' => [
                'token' => $request->token,
                'tries' => $request->tries,
                'completed' => $request->completed,
                'won' => $request->win_status,
                'claimed' => $request->claimed,
                'event_data' => $request->event_data
            ]
        ]);

        return json_decode((string) $response->getBody(), true);
    }

}