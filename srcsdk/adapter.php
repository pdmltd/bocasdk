<?php 

namespace Boca\Client;

require_once  __DIR__ .  '/../vendor/autoload.php';

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;


final class Adapter
{
    public function __construct()
    {

    }

    public function scratchard ($response) {

        if(!array_key_exists('previous_data', json_decode($response->getBody(), true))) {
            return json_decode((string) $response->getBody(), true);
        }
        
        if (!(json_decode($response->getBody(), true)['previous_data'])) {

            $all_prizes = json_decode($response->getBody(), true)['all_prizes'];
            $win_prize = json_decode($response->getBody(), true)['prize'];

            if ($win_prize['prize_type'] != 'no_win') {

                $return_array = array();
                $return_array = array_fill(1, 3, $win_prize);

                while (count($return_array) < 6) {
                    $random_item = $all_prizes[array_rand($all_prizes)];
                    if(!in_array($random_item, $return_array) &&  ($random_item['prize_type']!='no_win')){
                        array_push($return_array, $random_item);
                    }
                }

                shuffle($return_array);

                $index_count = 1;
                foreach ($return_array as &$item ) {
                    $item['index'] = $index_count;
                    $item['checked'] = false;
                    // $item['prize_img'] =  public_path() . '/images/scrtachcard/' . 'prize_' . $item['id'] . '.png';
                    $item['prize_img'] =   'src/images/prizes/' . 'prize_' . $item['id'] . '.png';
                    $item['prize_terms'] = $this->get_promos_terms($item['id']);
                    $index_count++;
                }
                $response_with_card_data = json_decode($response->getBody(), true);
                $response_with_card_data['fill_prizes'] = $return_array;
                // unset($response_with_card_data['prize']);
                unset($response_with_card_data['all_prizes']);
                return($response_with_card_data);
            }

            $win_prize = array (
                "id" => -1,
                "prize_info" => "",
                "prize_type" => ""
            );

            $return_array = array();

            while (count($return_array) < 6) {
                $random_item = $all_prizes[array_rand($all_prizes)];
                if(count(array_keys($return_array, $random_item)) < 2 &&  ($random_item['prize_type']!='no_win')){
                    array_push($return_array, $random_item);
                }
            }

            shuffle($return_array);

            $index_count = 1;
            foreach ($return_array as &$item ) {
                $item['index'] = $index_count;
                $item['checked'] = false;
                // $item['prize_img'] =   url('/images/scratchcard/' . 'prize_' . $item['id'] . '.png');
                $item['prize_img'] =   'src/images/prizes/' . 'prize_' . $item['id'] . '.png';
                $index_count++;
            }

            $response_with_card_data = json_decode($response->getBody(), true);
            $response_with_card_data['fill_prizes'] = $return_array;
            $response_with_card_data['prize'] = $win_prize;
            unset($response_with_card_data['all_prizes']);
            unset($response_with_card_data['previous_data']);
            return($response_with_card_data);

        }

        $response_decoded = json_decode($response->getBody(), true);

        if ($response_decoded['prize']['prize_type'] == 'no_win') {

            $win_prize = array (
                "id" => -1,
                "prize_info" => "",
                "prize_type" => ""
            );
            $response_decoded['prize'] = $win_prize;
        }

        $prev_data = $response_decoded['previous_data'];
        unset($response_decoded['all_prizes']);
        unset($response_decoded['previous_data']);
        $response_decoded['fill_prizes'] = json_decode(end($prev_data));
        return $response_decoded;
    }

    public function get_promos_terms($prize_id)
    {
        $terms_[1] = "T&C's : Spins valid for 7 days. 5 Free Spins on Irish luck. FS Wins paid in Games Bonus. FS wins are capped at £1 and are granted in games bonus after all FS are used.  GB Wagering X80 to withdraw games bonus & related winnings (max £100). Country restrictions apply. UK only - Your deposit balance (defined as Transaction Balance under our Withdrawal Policy) is available for withdrawal at any time. General Withdrawal Restrictions & T&Cs Apply.";
        $terms_[2] = "T&C's : Tickets valid for 7 days. 3 Free Tickets to on Wishful Thursday. Wins generated from bingo tickets are granted in bingo bonus and carry wagering requirements of x4. Country restrictions apply. UK only - Your deposit balance (defined as Transaction Balance under our Withdrawal Policy) is available for withdrawal at any time. General Withdrawal Restrictions & T&Cs Apply.";
        $terms_[3] = "T&C's : Tickets valid for 7 days. 1 Free Tickets to on The Big 10k. Wins generated from bingo tickets are granted in bingo bonus and carry wagering requirements of x4. Country restrictions apply. UK only - Your deposit balance (defined as Transaction Balance under our Withdrawal Policy) is available for withdrawal at any time. General Withdrawal Restrictions & T&Cs Apply.";
        $terms_[4] = "T&C's : Spins valid for 7 days. 10 Free Spins on Temple of Iris. FS Wins paid in Games Bonus. FS wins are capped at £3 and are granted in games bonus after all FS are used.  GB Wagering X80 to withdraw games bonus & related winnings (max £100). Country restrictions apply. UK only - Your deposit balance (defined as Transaction Balance under our Withdrawal Policy) is available for withdrawal at any time. General Withdrawal Restrictions & T&Cs Apply.";
        $terms_[5] = "T&C's : Tickets valid for 7 days. 10 Free Tickets to on BOUNC'T. Wins generated from bingo tickets are granted in bingo bonus and carry wagering requirements of x4. Country restrictions apply. UK only - Your deposit balance (defined as Transaction Balance under our Withdrawal Policy) is available for withdrawal at any time. General Withdrawal Restrictions & T&Cs Apply.";
        $terms_[6] = "This voucher entitles a player to £5 off any main evening session at your local Majestic Bingo club. Keep an eye out on your inbox for details. General Withdrawal Restrictions & T&Cs Apply.";
        $terms_[7] = "T&C's : Spins valid for 7 days. 3 Free Spins on Shamans Dream. FS Wins paid in Games Bonus. FS wins are capped at £0.50 and are granted in games bonus after all FS are used.  GB Wagering X80 to withdraw games bonus & related winnings (max £100). Country restrictions apply. UK only - Your deposit balance (defined as Transaction Balance under our Withdrawal Policy) is available for withdrawal at any time. General Withdrawal Restrictions & T&Cs Apply.";
        $terms_[8] = "T&C's : Spins valid for 7 days. 50 Free Spins on Fluffy Favourites. FS Wins paid in Games Bonus. FS wins are capped at £10 and are granted in games bonus after all FS are used.  GB Wagering X80 to withdraw games bonus & related winnings (max £100). Country restrictions apply. UK only - Your deposit balance (defined as Transaction Balance under our Withdrawal Policy) is available for withdrawal at any time. General Withdrawal Restrictions & T&Cs Apply.";
        $terms_[9] = "T&C's : Tickets valid for 7 days. 2 Free Tickets to on Mystery Jackpots. Wins generated from bingo tickets are granted in bingo bonus and carry wagering requirements of x4. Country restrictions apply. UK only - Your deposit balance (defined as Transaction Balance under our Withdrawal Policy) is available for withdrawal at any time.  General Withdrawal Restrictions & T&Cs Apply.";
        $terms_[10] = "";
        $terms_[11] = "Invited, funded (18+) players. Spins credited on 15/04/22, valid for 7 days. 6 Free Spins on Beez Kneez. FS max win cap: £2. FS Wins paid in Games Bonus. GB Wagering x80 to withdraw games bonus & related winnings (max £100). See below for full terms. Country restrictions apply. UK only - Deposit balance is available for withdrawal at any time.";
        $terms_[12] = "Invited, funded (18+) players. Spins credited on 16/04/22, valid for 7 days. 4 Free Spins on Fluffy Favourites. FS max win cap: £1. FS Wins paid in Games Bonus. GB Wagering x80 to withdraw games bonus & related winnings (max £100). See below for full terms. Country restrictions apply. UK only - Deposit balance is available for withdrawal at any time.";
        $terms_[13] = "Funded players only (+18). Valid until 23:59 17/04/22. Min Deposit £10. Promo code: EASTER. 12 Free Spins [FS]. FS are valid on Sugar Train for 7 days. Max winning is capped at £3. FS wins are granted in games bonus after all FS are used. To withdraw games bonus & related wins, wager x80 the amount of your bonus funds. Games bonus funds may be used on selected games only. Games Bonus wins cap: £100 + the initial bonus amount. Wagering req. vary by game. Country restrictions apply. UK only - deposit balance is available for withdrawal at any time.";
        $terms_[14] = "Invited, funded (18+) players. Spins credited on 18/04/22, valid for 7 days. 4 Free Spins on Shaman’s Dream 2. FS max win cap: £1. FS Wins paid in Games Bonus. GB Wagering x80 to withdraw games bonus & related winnings (max £100). See below for full terms. Country restrictions apply. UK only - Deposit balance is available for withdrawal at any time.";

        return $terms_[$prize_id];
    }
}